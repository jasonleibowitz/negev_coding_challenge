class ApplicantController < ApplicationController

  def new

  end

  def create
    @applicant = Applicant.new(applicant_params)
    if @applicant.valid?
      @applicant.save
      flash[:notice] = "Congratulations and Thank you for your application. Someone will be in touch with you shortly."
      redirect_to apply_path
    else
      flash[:error] = "There has been an error. Please try again."
      redirect_to apply_path
    end
  end

  private
  def applicant_params
    params.require(:applicant).permit(:name, :email, :phone, :movein_date, :linkedin, :facebook, :twitter)
  end

end
