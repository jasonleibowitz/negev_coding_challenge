$( document ).ready(function() {
  console.log("Loaded, bro");

  $(window).scroll(function(){
    var scrolled = $(window).scrollTop();
    var applyButtonTop = $('#apply-button').position().top;
    if (scrolled > applyButtonTop){
      $('#top-apply').fadeIn();
    } else if (scrolled < applyButtonTop) {
      $('#top-apply').fadeOut();
    }
  });

  $('.faq-question').click(function(){
    var num = $(this).data('num');
    if ($('#answer-' + num).css('display') === 'none'){
      $('#answer-' + num).slideDown();
    } else {
      $('#answer-' + num).slideUp();
    }
  });

  $('#showApplication').click(function(){
    $('#app-form').fadeIn('slow');
    $('html,body').animate({scrollTop: $('#app-form').offset().top},'slow');
  });

});

