class CreateApplicant < ActiveRecord::Migration
  def change
    create_table :applicants do |t|
      t.string :name
      t.string :email
      t.integer :phone
      t.date :movein_date
      t.string :linkedin
      t.string :facebook
      t.string :twitter
    end
  end
end
