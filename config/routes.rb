Rails.application.routes.draw do
  root to: 'home#welcome'
  get '/apply', to: 'home#apply'
  get '/faq', to: 'home#faq'
  resources :applicant
end
